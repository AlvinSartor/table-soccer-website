﻿using System;

namespace Core
{
    /// <summary>
    /// Describes the possible outcomes of a match.
    /// </summary>
    public enum MatchResult
    {
        Victory = 2,
        Draw = 1,
        Loss = 0
    }

    public static class EnumExtensions
    {
        /// <summary>
        /// Converts the specified result to ranking points.
        /// </summary>
        /// <param name="result">The result.</param>
        public static int ToPoints(this MatchResult result) =>
            result switch
            {
                MatchResult.Victory => 2,
                MatchResult.Draw => 1,
                MatchResult.Loss => 0,
                _ => throw new ArgumentOutOfRangeException(nameof(result), result, null)
            };
    }
}
