using System;
using System.Linq;
using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class StatisticsFixture
    {
        [Test]
        public void RankingCanBeExtractedFromStatistics()
        {
            var statistics = new Statistics();

            var ying = new Player("Ying");
            var ceesjan = new Player("Ceesjan");
            var john = new Player("John");

            var teamYing = new Team(new[] {ying});
            var teamCeesjan = new Team(new[] {ceesjan});
            var teamJohn = new Team(new[] {john});
            var teamYingJohn = new Team(new[] {ying, john});
            
            statistics.AddTeam(teamYing);
            statistics.AddTeam(teamCeesjan);
            statistics.AddTeam(teamJohn);
            statistics.AddTeam(teamYingJohn);

            var match1 = new Match(teamYing, teamCeesjan, 10, 8, DateTime.Today);
            var match2 = new Match(teamCeesjan, teamYingJohn, 10, 6, DateTime.Today);
            var match3 = new Match(teamJohn, teamYing, 4, 4, DateTime.Today);

            statistics.AddMatch(match1);
            statistics.AddMatch(match2);
            statistics.AddMatch(match3);

            var ranking = statistics.GetRanking();

            var first = ranking.First();
            Assert.That(first, Is.EqualTo((teamYing, 3, 2)));

            var second = ranking.Skip(1).First();
            Assert.That(second, Is.EqualTo((teamCeesjan, 2, 2)));

            var third = ranking.Skip(2).First();
            Assert.That(third, Is.EqualTo((teamJohn, 1, 1)));

            var last = ranking.Last();
            Assert.That(last, Is.EqualTo((teamYingJohn, 0, 1)));
        }

        [Test]
        public void SmallerTeamsAreRankedHigherThanSamePointsBiggerTeams()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today);
            statistics.AddMatch(match1);


            var ranking = statistics.GetRanking();

            var first = ranking.First();
            Assert.That(first, Is.EqualTo((t1, 1, 1)));
            var second = ranking.Skip(1).First();
            Assert.That(second, Is.EqualTo((t2, 1, 1)));


            var match2 = new Match(t1, t2, 9, 9, DateTime.Today);
            statistics.AddMatch(match2);

            ranking = statistics.GetRanking();

            first = ranking.First();
            Assert.That(first, Is.EqualTo((t1, 2, 2)));
            second = ranking.Skip(1).First();
            Assert.That(second, Is.EqualTo((t2, 2, 2)));

            // bonus: same points but less matches are ranked higher

            var t3 = new Team(new[] { new Player("P4") });
            var t4 = new Team(new[] { new Player("P5") });
            statistics.AddTeam(t3);
            statistics.AddTeam(t4);

            var match3 = new Match(t3, t4, 10, 8, DateTime.Today);
            statistics.AddMatch(match3);

            ranking = statistics.GetRanking();

            first = ranking.First();
            Assert.That(first, Is.EqualTo((t3, 2, 1)));
            second = ranking.Skip(1).First();
            Assert.That(second, Is.EqualTo((t1, 2, 2)));
            var third = ranking.Skip(2).First();
            Assert.That(third, Is.EqualTo((t2, 2, 2)));
            var fourth = ranking.Last();
            Assert.That(fourth, Is.EqualTo((t4, 0, 1)));
        }

        [Test]
        public void ItIsPossibleToInquireForExistenceOfStatisticsInAGivenPeriod()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today);
            statistics.AddMatch(match1);

            Assert.True(statistics.HasStatisticsForTheSelectedTimeSpan(DateTime.Today.AddDays(-5), DateTime.Today));
            Assert.False(statistics.HasStatisticsForTheSelectedTimeSpan(DateTime.Today.AddDays(-10), DateTime.Today.AddDays(-5)));

            Assert.True(statistics.HasStatisticsForTheSelectedTimeSpan(null, null)); // Unix time till now.
        }

        [Test]
        public void ItIsPossibleToInquireForExistenceOfTeamStatisticsInAGivenPeriod()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            var t3 = new Team(new[] { new Player("P4") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);
            statistics.AddTeam(t3);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today);
            statistics.AddMatch(match1);

            Assert.False(statistics.TeamHasStatisticsForTheSelectedTimeSpan(t3, DateTime.Today.AddDays(-5), DateTime.Today));

            var match3 = new Match(t3, t2, 9, 9, DateTime.Today);
            statistics.AddMatch(match3);

            Assert.True(statistics.TeamHasStatisticsForTheSelectedTimeSpan(t3, DateTime.Today.AddDays(-5), DateTime.Today));
            Assert.True(statistics.TeamHasStatisticsForTheSelectedTimeSpan(t3, null, null)); // Unix time till now.
        }

        [Test]
        public void ItIsPossibleToCalculateOverallStatistics()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            var t3 = new Team(new[] { new Player("P4") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);
            statistics.AddTeam(t3);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today.AddDays(-1));
            statistics.AddMatch(match1);
            var match3 = new Match(t3, t2, 9, 2, DateTime.Today);
            statistics.AddMatch(match3);

            var yesterdayStats = new TeamStats(2, 18, 18, 1, 0, 1, 0);
            var todayStats = new TeamStats(2, 11, 11, 1, 1, 0, 1);
            var overallStats = new TeamStats(4, 29, 29, 2, 1, 1, 1);

            var today = DateTime.Today;
            var yesterday = today.AddDays(-1);
            
            Assert.That(statistics.CalculateOverallStatistics(), Is.EqualTo(overallStats));
            Assert.That(statistics.CalculateOverallStatistics(today, today), Is.EqualTo(todayStats));
            Assert.That(statistics.CalculateOverallStatistics(yesterday, yesterday), Is.EqualTo(yesterdayStats));
        }

        [Test]
        public void ItIsPossibleToCalculateTeamStatistics()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            var t3 = new Team(new[] { new Player("P4") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);
            statistics.AddTeam(t3);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today.AddDays(-1));
            statistics.AddMatch(match1);
            var match3 = new Match(t3, t2, 9, 2, DateTime.Today);
            statistics.AddMatch(match3);

            var t2YesterdayStats = new TeamStats(1, 9, 9, 1, 0, 1, 0);
            var t2TodayStats = new TeamStats(0, 2, 9, 1, 0, 0, 1);
            var t2OverallStats = new TeamStats(1, 11, 18, 2, 0, 1, 1);

            var today = DateTime.Today;
            var yesterday = today.AddDays(-1);

            Assert.That(statistics.CalculateStatistics(t2), Is.EqualTo(t2OverallStats));
            Assert.That(statistics.CalculateStatistics(t2, today, today), Is.EqualTo(t2TodayStats));
            Assert.That(statistics.CalculateStatistics(t2, yesterday, yesterday), Is.EqualTo(t2YesterdayStats));
        }

        [Test]
        public void ItIsPossibleToCalculateDailyProgress()
        {
            var statistics = new Statistics();

            var t1 = new Team(new[] { new Player("P1") });
            var t2 = new Team(new[] { new Player("P3"), new Player("P2") });
            var t3 = new Team(new[] { new Player("P4") });
            statistics.AddTeam(t1);
            statistics.AddTeam(t2);
            statistics.AddTeam(t3);

            var match1 = new Match(t1, t2, 9, 9, DateTime.Today.AddDays(-1));
            var match3 = new Match(t3, t2, 9, 2, DateTime.Today);
            statistics.AddMatch(match1);
            statistics.AddMatch(match3);

            var today = DateTime.Today;
            var yesterday = today.AddDays(-1);

            var t2YesterdayProgress = new[] {(yesterday, 1)};
            var t2TodayProgress = new[] { (today, 0) };
            var t2OverallProgress = new[] { (yesterday, 1), (today, 1) };

            CollectionAssert.AreEquivalent(t2OverallProgress, statistics.GetDailyProgress(t2));
            CollectionAssert.AreEquivalent(t2TodayProgress, statistics.GetDailyProgress(t2, today, today));
            CollectionAssert.AreEquivalent(t2YesterdayProgress, statistics.GetDailyProgress(t2, yesterday, yesterday));
        }
    }
}