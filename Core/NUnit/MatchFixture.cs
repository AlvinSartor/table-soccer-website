using System;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class MatchFixture
    {
        [Test]
        public void MatchesCanBeSerializedAndDeserializedBack()
        {
            var team1 = new Team(new[] { new Player("Andonio")}, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca")}, "We Never Train");
            var match = new Match(team1, team2, 2, 0, DateTime.Today);

            var json = JsonConvert.SerializeObject(match);
            var match2 = JsonConvert.DeserializeObject<Match>(json);

            Assert.That(match, Is.EqualTo(match2));
        }

        [Test]
        public void MatchWinnerIsIdentified()
        {
            var team1 = new Team(new[] { new Player("Andonio") }, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca") }, "We Never Train");
            
            var match1 = new Match(team1, team2, 2, 0, DateTime.Today);
            Assert.That(match1.GetResultFor(team1), Is.EqualTo(MatchResult.Victory));
            Assert.That(match1.GetResultFor(team2), Is.EqualTo(MatchResult.Loss));

            var match2 = new Match(team1, team2, 2, 2, DateTime.Today.AddDays(1));
            Assert.That(match2.GetResultFor(team1), Is.EqualTo(MatchResult.Draw));
            Assert.That(match2.GetResultFor(team2), Is.EqualTo(MatchResult.Draw));

            Assert.Throws<ArgumentException>(() => match2.GetResultFor(new Team(new[] { new Player("Luca")})));
        }

        [Test]
        public void ItIsPossibleToCheckIfTeamParticipatedToMatch()
        {
            var team1 = new Team(new[] { new Player("Andonio") }, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca") }, "We Never Train");
            var team3 = new Team(new[] { new Player("Martijn") }, "Mega Team");

            var match = new Match(team1, team2, 2, 0, DateTime.Today);
            
            Assert.That(match.Contains(team1), Is.True);
            Assert.That(match.Contains(team2), Is.True);
            Assert.That(match.Contains(team3), Is.False);
        }

        [Test]
        public void MatchIsSetAsCompletedWhenScoreIsAdded()
        {
            var team1 = new Team(new[] { new Player("Andonio") }, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca") }, "We Never Train");
            var match = new Match(team1, team2, null, null, DateTime.Today);

            Assert.That(match.Completed, Is.False);

            match.EndMatch(2, 0);
            Assert.That(match.Completed, Is.True);
        }

        [Test]
        public void ItIsPossibleToCheckIfMatchEndedWithADrawResult()
        {
            var team1 = new Team(new[] { new Player("Andonio") }, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca") }, "We Never Train");
            var team3 = new Team(new[] { new Player("Martijn") }, "Mega Team");

            var match1 = new Match(team1, team2, 2, 0, DateTime.Today);
            var match2 = new Match(team2, team3, 0, 0, DateTime.Today);

            Assert.That(match1.IsDraw, Is.False);
            Assert.That(match2.IsDraw, Is.True);
        }


        [Test]
        public void ItIsPossibleToGetTheGoalStatsForATeamThatParticipatedToTheMatch()
        {
            var team1 = new Team(new[] { new Player("Andonio") }, "Super Team");
            var team2 = new Team(new[] { new Player("Zaira"), new Player("Luca") }, "We Never Train");

            var match1 = new Match(team1, team2, 2, 7, DateTime.Today);

            Assert.That(match1.GetGoalsStats(team1), Is.EqualTo((7, 2)));
            Assert.That(match1.GetGoalsStats(team2), Is.EqualTo((2, 7)));
        }
    }
}