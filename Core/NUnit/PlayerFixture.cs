using Newtonsoft.Json;
using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class PlayerFixture
    {
        [Test]
        public void PlayerCanBeSerializedAndDeserializedBack()
        {
            var player = new Player("Andonio");
            var json = JsonConvert.SerializeObject(player);
            var player2 = JsonConvert.DeserializeObject<Player>(json);

            Assert.That(player, Is.EqualTo(player2));
        }
    }
}