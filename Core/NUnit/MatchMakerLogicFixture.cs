using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Core.NUnit
{
    [TestFixture]
    internal sealed class MatchMakerLogicFixture
    {
        [Test]
        public void TeamsAreAssociatedInBaseOnTheStatistics()
        {
            var teamA = new Team(new []{new Player("A")});
            var teamAStats = new TeamStats(25, 30, 7, 5, 5, 0, 0);

            var teamB = new Team(new[] { new Player("B") });
            var teamBStats = new TeamStats(27, 29, 8, 6, 5, 1, 0);

            var teamC = new Team(new[] { new Player("C") });
            var teamCStats = new TeamStats(6, 5, 35, 6, 0, 5, 1);

            var teamD = new Team(new[] { new Player("D") });
            var teamDStats = new TeamStats(8, 8, 11, 7, 1, 5, 5);

            var collection = new Dictionary<Team, TeamStats>
            {
                {teamA, teamAStats},
                {teamB, teamBStats},
                {teamC, teamCStats},
                {teamD, teamDStats},
            };

            var matches = MatchMakerLogic.CreateBalancedMatches(collection);

            Assert.That(matches.SingleOrDefault(x => x.Contains(teamA) && x.Contains(teamB)), Is.Not.Null);
            Assert.That(matches.SingleOrDefault(x => x.Contains(teamC) && x.Contains(teamD)), Is.Not.Null);
        }

        [Test]
        public void TeamsWithSharedMembersAreNotPlacedInMatch()
        {
            var sharedPlayer = new Player("X");

            var teamA = new Team(new[] { new Player("A"), sharedPlayer });
            var teamAStats = new TeamStats(25, 30, 7, 5, 5, 0, 0);

            var teamB = new Team(new[] { new Player("B"), sharedPlayer });
            var teamBStats = new TeamStats(27, 29, 8, 6, 5, 1, 0);


            var collection = new Dictionary<Team, TeamStats>
            {
                {teamA, teamAStats},
                {teamB, teamBStats},
            };

            var matches = MatchMakerLogic.CreateBalancedMatches(collection);

            Assert.That(matches, Is.Empty);
        }
    }
}