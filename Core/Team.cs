﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Newtonsoft.Json;

namespace Core
{
    public sealed class Team
    {
        /// <summary>
        /// The team identifier.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// The team name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The team members.
        /// </summary>
        public ImmutableList<Player> Members { get; }

        /// <summary>
        /// The names of the members of the team.
        /// </summary>
        public string MemberNames =>
            Members.Select(x => x.Name).OrderBy(x => x).Aggregate("", (s, player) => s + player + "+")[..^1];

        /// <summary>
        /// Initializes a new instance of <see cref="Team"/>
        /// </summary>
        /// <param name="members">The members of the team.</param>
        /// <param name="name">The team name. If omitted, the name will be set as the (alphabetically sorted) names of the members.</param>
        public Team(IEnumerable<Player> members, string name = "")
        {
            var teamMembers = members.ToImmutableList();
            if (teamMembers.Count == 0 || teamMembers.Count > 3)
                throw new ArgumentException("A team should be composed by 1 to 3 members.");

            Id = Guid.NewGuid().ToString();
            Members = teamMembers;
            Name = string.IsNullOrEmpty(name)
                ? Members.Select(x => x.Name).OrderBy(x => x).Aggregate("", (s, player) => s + player + "+")[..^1]
                : name;
        }


        [JsonConstructor]
        private Team(string id, IEnumerable<Player> members, string name)
        {
            Id = id;
            Members = members.ToImmutableList();
            Name = name;
        }

        /// <summary>
        /// Determines whether this team has common members with the specified other team.
        /// </summary>
        /// <param name="otherTeam">The other team.</param>
        public bool HasCommonMembersWith(Team otherTeam) => Members.Any(player => otherTeam.Members.Contains(player));

        /// <inheritdoc />
        public override bool Equals(object? obj) =>
            obj is Team other && Equals(other);

        public bool Equals(Team? other)
        {
            if (other == null) return false;
            if (!Id.Equals(other.Id) || Name != other.Name) return false;
            
            var membersIds = Members.Select(x => x.Id).OrderBy(x => x).Aggregate("", (s, s1) => s + s1);
            var otherIds = other.Members.Select(x => x.Id).OrderBy(x => x).Aggregate("", (s, s1) => s + s1);
            return membersIds == otherIds;
        }

        /// <inheritdoc />
        public override int GetHashCode() =>
            HashCode.Combine(Id, Name, Members);

        /// <inheritdoc />
        public override string ToString() =>
            Name;
    }
}