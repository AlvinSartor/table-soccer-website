﻿using System;

namespace Core
{
    public sealed class TeamStats
    {
        public int PointsScored { get; }
        public int GoalsScored { get; }
        public int GoalsTaken { get; }
        public int GamesPlayed { get; }
        public int Victories { get; }
        public int Draws { get; }
        public int Losses { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamStats"/> class.
        /// </summary>
        /// <param name="pointsScored">The points scored.</param>
        /// <param name="goalsScored">The goals scored.</param>
        /// <param name="goalsTaken">The goals taken.</param>
        /// <param name="gamesPlayed">The games played.</param>
        /// <param name="victories">The number of victories.</param>
        /// <param name="draws">The number of draws.</param>
        /// <param name="losses">The number of losses.</param>
        public TeamStats(int pointsScored, int goalsScored, int goalsTaken, int gamesPlayed, int victories, int draws, int losses)
        {
            PointsScored = pointsScored;
            GoalsScored = goalsScored;
            GamesPlayed = gamesPlayed;
            Victories = victories;
            Draws = draws;
            Losses = losses;
            GoalsTaken = goalsTaken;
        }

        /// <summary>
        /// Gets the similarity between two statistics (0 is maximum similarity).
        /// </summary>
        /// <param name="other">The other statistics.</param>
        public double GetSimilarity(TeamStats other)
        {
            var points = PointsScored / (double)GamesPlayed - other.PointsScored / (double)other.GamesPlayed;
            var goals = GoalsScored / ((double)GoalsTaken + 1) - other.GoalsScored / ((double)other.GoalsTaken + 1);
            var victories = Victories / (double) GamesPlayed - other.Victories / (double) other.GamesPlayed;

            return Math.Abs(points) + Math.Abs(goals) + Math.Abs(victories);
        }

        public bool Equals(TeamStats? other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            return PointsScored == other.PointsScored 
                   && GoalsScored == other.GoalsScored 
                   && GoalsTaken == other.GoalsTaken 
                   && GamesPlayed == other.GamesPlayed 
                   && Victories == other.Victories 
                   && Draws == other.Draws 
                   && Losses == other.Losses;
        }

        /// <inheritdoc />
        public override bool Equals(object? obj) => 
            ReferenceEquals(this, obj) || obj is TeamStats other && Equals(other);

        /// <inheritdoc />
        public override int GetHashCode() => 
            HashCode.Combine(PointsScored, GoalsScored, GoalsTaken, GamesPlayed, Victories, Draws, Losses);
    }
}
