﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public sealed class Statistics
    {
        private readonly HashSet<Team> _teams;
        private readonly List<Match> _matches;

        /// <summary>
        /// Initializes a new instance of the <see cref="Statistics"/> class.
        /// </summary>
        public Statistics()
        {
            _teams = new HashSet<Team>();
            _matches = new List<Match>();
        }

        /// <summary>
        /// Adds the specified team.
        /// </summary>
        /// <param name="team">The team to add.</param>
        public void AddTeam(Team team) => _teams.Add(team);

        /// <summary>
        /// Adds the match.
        /// </summary>
        /// <param name="match">The match to add.</param>
        public void AddMatch(Match match) => _matches.Add(match);

        /// <summary>
        /// Determines whether there are statistics for the selected time span.
        /// </summary>
        /// <param name="fromDate">Initial date. If <c>null</c> the initial of Unix time will be selected.</param>
        /// <param name="toDate">Final date. If <c>null</c> the DateTime.Now will be selected.</param>
        public bool HasStatisticsForTheSelectedTimeSpan(DateTime? fromDate, DateTime? toDate)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            return _matches.Any(x => x.Completed && x.Date <= toDate && x.Date >= fromDate);
        }

        /// <summary>
        /// Determines whether the specified team has statistics for the selected time span.
        /// </summary>
        /// <param name="team">The team.</param>
        /// <param name="fromDate">Initial date. If <c>null</c> the initial of Unix time will be selected.</param>
        /// <param name="toDate">Final date. If <c>null</c> the DateTime.Now will be selected.</param>
        public bool TeamHasStatisticsForTheSelectedTimeSpan(Team team, DateTime? fromDate, DateTime? toDate)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            return _matches.Any(x => x.Completed && x.Contains(team) && x.Date <= toDate && x.Date >= fromDate);
        }

        /// <summary>
        /// Gets the overall ranking.
        /// </summary>
        public List<(Team team, int points, int matches)> GetRanking(DateTime? fromDate = null, DateTime? toDate = null)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            var ranking = from team in _teams
                let playedMatches = _matches
                    .Where(x => x.Completed && x.Contains(team) && x.Date <= toDate && x.Date >= fromDate).ToList()
                select (
                    team,
                    points: playedMatches.Sum(x => x.GetResultFor(team).ToPoints()),
                    matches: playedMatches.Count);

            return ranking.OrderByDescending(x => x.points)
                .ThenBy(x => x.team.Members.Count)
                .ThenBy(x => x.matches)
                .ToList();
        }

        /// <summary>
        /// Calculates the statistics for the specified team.
        /// </summary>
        /// <param name="team">The team.</param>
        /// <param name="fromDate">The initial date.</param>
        /// <param name="toDate">The final date.</param>
        public TeamStats CalculateStatistics(Team team, DateTime? fromDate = null, DateTime? toDate = null)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            var matches = _matches.Where(x => x.Date >= fromDate && x.Date <= toDate && x.Contains(team) && x.Completed)
                .ToList();

            var nrMatches = matches.Count;
            var nrPoints = matches.Sum(x => x.GetResultFor(team).ToPoints());

            var goalStats = matches.Select(x => x.GetGoalsStats(team)).ToList();
            var goalScored = goalStats.Sum(x => x.scored);
            var goalReceived = goalStats.Sum(x => x.received);

            var victories = matches.Count(x => x.GetResultFor(team) == MatchResult.Victory);
            var draws = matches.Count(x => x.GetResultFor(team) == MatchResult.Draw);
            var loss = matches.Count(x => x.GetResultFor(team) == MatchResult.Loss);

            return new TeamStats(nrPoints, goalScored, goalReceived, nrMatches, victories, draws, loss);
        }

        /// <summary>
        /// Calculates the overall statistics for the specified time span.
        /// </summary>
        /// <param name="fromDate">The initial date.</param>
        /// <param name="toDate">The final date.</param>
        public TeamStats CalculateOverallStatistics(DateTime? fromDate = null, DateTime? toDate = null)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            var matches = _matches.Where(x => x.Date >= fromDate && x.Date <= toDate && x.Completed).ToList();

            var nrMatches = matches.Count;
            var nrPoints = nrMatches * 2;

            var goalScored = matches.Sum(x => x.VisitorScore + x.HomeScore);
            var goalReceived = goalScored;

            var draws = matches.Count(x => x.IsDraw);
            var victories = nrMatches - draws;
            var loss = victories;

            return new TeamStats(nrPoints, goalScored, goalReceived, nrMatches, victories, draws, loss);
        }

        /// <summary>
        /// Gets the daily progress of the specified team.
        /// </summary>
        /// <param name="team">The team.</param>
        /// <param name="fromDate">The initial date.</param>
        /// <param name="toDate">The final date.</param>
        public (DateTime date, int points)[] GetDailyProgress(Team team, DateTime? fromDate = null, DateTime? toDate = null)
        {
            fromDate ??= DateTime.FromFileTimeUtc(0);
            toDate ??= DateTime.Today;

            var pointsPerDay = _matches
                .Where(x => x.Date >= fromDate && x.Date <= toDate && x.Completed)
                .GroupBy(x => x.Date)
                .Select(x => (date: x.Key, points: x.Sum(y => y.Contains(team) ? y.GetResultFor(team).ToPoints() : 0)))
                .OrderBy(x => x.date)
                .ToArray();

            if (pointsPerDay.Length > 0)
            {
                // accumulating points
                for (var i = 1; i < pointsPerDay.Length; i++)
                    pointsPerDay[i] = (pointsPerDay[i].date, pointsPerDay[i].points + pointsPerDay[i - 1].points);
            }

            return pointsPerDay;
        }
    }
}