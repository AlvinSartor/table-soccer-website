﻿using System;
using System.Linq;
using Core;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace TableSoccer.Pages
{
    public class TeamStatsModel : PageModel
    {
        private (DateTime date, int points)[] _dailyProgress;
        private (DateTime date, int points)[] _currentWeekProgress;

        /// <summary>
        /// Gets a value indicating whether this team has statistics.
        /// </summary>
        public bool HasStats => SoccerTableManager.Instance().TeamHasStats(Team);

        /// <summary>
        /// Gets a value indicating whether this team has statistics relative to the current week.
        /// </summary>
        public bool HasCurrentWeekStatistics => SoccerTableManager.Instance().TeamHasStatsForCurrentWeek(Team);

        /// <summary>
        /// Gets the team.
        /// </summary>
        public Team Team { get; private set; }

        /// <summary>
        /// Gets the team statistics.
        /// </summary>
        public TeamStats TeamStatistics => SoccerTableManager.Instance().TeamStats(Team);

        /// <summary>
        /// Gets the team statistics relative to the current week.
        /// </summary>
        public TeamStats CurrentWeekTeamStatistics => SoccerTableManager.Instance().CurrentWeekTeamStats(Team);

        /// <summary>
        /// Gets the current week dates in string format.
        /// </summary>
        public string CurrentWeekDates
        {
            get
            {
                var (weekBeginning, weekEnding) = SoccerTableManager.Instance().GetCurrentWeek();
                return $"{weekBeginning:dd/MM} - {weekEnding:dd/MM}";
            }
        }

        /// <summary>
        /// Values for the X axis of the overall progresses chart.
        /// </summary>
        public string ProgressDatesString() =>
            _dailyProgress.Select(x => x.date).Aggregate("", (s, date) => s + DateToString(date) + ", ")[..^2];

        /// <summary>
        /// Values for the Y axis of the overall progresses chart.
        /// </summary>
        public string ProgressPointsString() =>
            _dailyProgress.Select(x => x.points).Aggregate("", (s, points) => s + points + ", ")[..^2];

        /// <summary>
        /// Values for the X axis of the current week progresses chart.
        /// </summary>
        public string CurrentWeekProgressDatesString() =>
            _currentWeekProgress.Select(x => x.date).Aggregate("", (s, date) => s + DateToString(date) + ", ")[..^2];

        /// <summary>
        /// Values for the Y axis of the current week progresses chart.
        /// </summary>
        public string CurrentWeekProgressPointsString() =>
            _currentWeekProgress.Select(x => x.points).Aggregate("", (s, points) => s + points + ", ")[..^2];

        private static string DateToString(DateTime d) => $"\"{d:dd MMM}\"";

        /// <summary>
        /// Called when [get].
        /// </summary>
        public void OnGet()
        {
            if (!Request.Query.ContainsKey("id")) return;

            Team = SoccerTableManager.Instance().Teams().FirstOrDefault(x => x.Id == Request.Query["id"].ToString());
            if (Team == null) return;

            if (SoccerTableManager.Instance().TeamHasStats(Team))
                _dailyProgress = SoccerTableManager.Instance().DailyProgress(Team);
            
            if (SoccerTableManager.Instance().TeamHasStatsForCurrentWeek(Team))
                _currentWeekProgress = SoccerTableManager.Instance().CurrentWeekProgress(Team);
        }
    }
}