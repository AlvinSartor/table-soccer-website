﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace TableSoccer.Pages
{
    public class StatisticsModel : PageModel
    {
        /// <summary>
        /// Gets a value indicating whether there are registered matches.
        /// </summary>
        public bool HasMatches { get; private set; }

        /// <summary>
        /// Gets a value indicating whether there are registered matches this week.
        /// </summary>
        public bool HasMatchesThisWeek { get; private set; }

        /// <summary>
        /// Gets the list of all teams.
        /// </summary>
        public IEnumerable<Team> Teams => SoccerTableManager.Instance().Teams();

        /// <summary>
        /// Gets the dates of the current week.
        /// </summary>
        public string CurrentWeekDates
        {
            get
            {
                var (weekBeginning, weekEnding) = SoccerTableManager.Instance().GetCurrentWeek();
                return $"{weekBeginning:dd/MM} - {weekEnding:dd/MM}";
            }
        }

        /// <summary>
        /// Gets the general ranking.
        /// </summary>
        public List<(Team team, int points, int matches)> Ranking() => 
            SoccerTableManager.Instance().Ranking();

        /// <summary>
        /// Gets the current week ranking.
        /// </summary>
        public List<(Team team, int points, int matches)> CurrentWeekRanking() => 
            SoccerTableManager.Instance().CurrentWeekRanking();

        /// <summary>
        /// Gets the general overalls stats for all teams.
        /// </summary>
        public TeamStats OverallStats() => SoccerTableManager.Instance().OverallStats();

        /// <summary>
        /// Gets the general current week stats.
        /// </summary>
        public TeamStats CurrentWeekStats() => SoccerTableManager.Instance().CurrentWeekStats();

        private static string DateToString(DateTime d) => $"\"{d:dd MMM}\"";

        /// <summary>
        /// Gets the overall progress dates (used as labels for the chart).
        /// </summary>
        public string OverallProgressDatesString { get; private set; }

        /// <summary>
        /// Gets the current week progress dates (used as labels for the chart).
        /// </summary>
        public string CurrentWeekProgressDatesString { get; private set; }

        /// <summary>
        /// Gets the data sets, relative to the whole period, for all teams.
        /// </summary>
        public string OverallDataSets() => 
            Teams.Aggregate("", (s, team) => s + CreateDataSet(team, true) + ", ")[..^2];

        /// <summary>
        /// Gets the data sets, relative to the current week, for all teams.
        /// </summary>
        public string CurrentWeekDataSets()
        {
            return Teams.Aggregate("", (s, team) => s + CreateDataSet(team, false) + ", ")[..^2];
        }

        private static string CreateDataSet(Team team, bool overall)
        {
            var (r, g, b) = RandomColor();

            return @"{
            label: '" + team.Name + @"',
            borderWidth: 2,
            data: [" + GetProgressesForTeam(team, overall) + @"],
            borderColor:'rgba(" + r + "," + g + "," + b + @", 1)',
            backgroundColor:'rgba(" + r + "," + g + "," + b + @", 0.05)'
            }";
        }

        private static string GetProgressesForTeam(Team team, bool overall)
        {
            var progresses = overall
                ? SoccerTableManager.Instance().DailyProgress(team)
                : SoccerTableManager.Instance().CurrentWeekProgress(team);

            return progresses
                .Select(x => x.points)
                .Aggregate("", (s, points) => s + points + ", ")[..^2];
        }

        private static (int r, int g, int b) RandomColor()
        {
            var r = new Random();
            return (120, r.Next(175, 210), r.Next(256));
        }

        /// <summary>
        /// Called when [get].
        /// </summary>
        public void OnGet()
        {
            var matches = SoccerTableManager.Instance().Matches();
            HasMatches = matches.Count > 0;
            if (!HasMatches) return;

            var dates = matches.Select(x => x.Date).ToHashSet().OrderBy(x => x).ToList();
            OverallProgressDatesString = dates.Aggregate("", (s, date) => s + DateToString(date) + ", ")[..^2];

            var (weekBeginning, weekEnding) = SoccerTableManager.Instance().GetCurrentWeek();
            var currentWeekDates = matches.Select(x => x.Date)
                .Where(x => x >= weekBeginning && x <= weekEnding).ToHashSet().OrderBy(x => x).ToList();

            HasMatchesThisWeek = currentWeekDates.Count > 0;
            if (!HasMatchesThisWeek) return;

            CurrentWeekProgressDatesString = currentWeekDates.Aggregate("", (s, date) => s + DateToString(date) + ", ")[..^2];
        }
    }
}