﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace TableSoccer.Pages
{
    public class MatchMakerModel : PageModel
    {
        /// <summary>
        /// Gets the team list.
        /// </summary>
        public List<Team> Teams => SoccerTableManager.Instance().Teams();

        /// <summary>
        /// Gets the list of proposed matches.
        /// </summary>
        public List<Match> ProposedMatches { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether this instance has proposed matches.
        /// </summary>
        public bool HasProposedMatches => ProposedMatches != null;

        /// <summary>
        /// Called when [post make matches].
        /// </summary>
        public void OnPostMakeMatches()
        {
            var selectedTeams = Teams
                .Where(x => Request.Form.ContainsKey(x.Id))
                .ToDictionary(team => team, team => SoccerTableManager.Instance().TeamStats(team));

            ProposedMatches = MatchMakerLogic.CreateBalancedMatches(selectedTeams);
        }
    }
}