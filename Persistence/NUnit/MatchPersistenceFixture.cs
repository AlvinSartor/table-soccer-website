using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using NUnit.Framework;

namespace Persistence.NUnit
{
    [TestFixture]
    internal sealed class MatchPersistenceFixture
    {
        private MatchPersistence? _persistence;

        [SetUp]
        public void SetUp()
        {
            _persistence = new MatchPersistence(true);
        }

        [TearDown]
        public void TearDown()
        {
            _persistence?.Dispose();
        }

        [Test]
        public void MatchesAreStoredAndRetrieved()
        {
            var team1 = new Team(new[] { new Player("Juan"), new Player("Luis") });
            var team2 = new Team(new[] { new Player("Betty") }, "SuperBetty");

            var match1 = new Match(team1, team2, 10, 0, DateTime.Today);
            var match2 = new Match(team2, team1, 7, 12, DateTime.Today);

            _persistence!.StoreMatch(match1);
            _persistence!.StoreMatch(match2);

            CollectionAssert.AreEquivalent(new[] { match1, match2 }, _persistence.FetchAllMatches());
        }

        [Test]
        public void AddingSameMatchTwiceCausesException()
        {
            var team1 = new Team(new[] { new Player("Juan"), new Player("Luis") });
            var team2 = new Team(new[] { new Player("Betty") }, "SuperBetty");

            var match1 = new Match(team1, team2, 10, 0, DateTime.Today);

            Assert.DoesNotThrow(() => _persistence!.StoreMatch(match1));
            Assert.Throws<Exception>(() => _persistence!.StoreMatch(match1));
        }

        [Test]
        public void MatchesCanBeDeleted()
        {
            var team1 = new Team(new[] { new Player("Juan"), new Player("Luis") });
            var team2 = new Team(new[] { new Player("Betty") }, "SuperBetty");

            var match1 = new Match(team1, team2, 10, 0, DateTime.Today);

            _persistence!.StoreMatch(match1);
            _persistence.RemoveMatch(match1);

            Assert.That(_persistence.FetchAllMatches().Count(), Is.EqualTo(0));
        }
    }
}