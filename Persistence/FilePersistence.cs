﻿using System;
using System.IO;

namespace Persistence
{
    public abstract class FilePersistence : IDisposable
    {
        private readonly bool _testMode;

        /// <summary>
        /// Gets the name of the file where the information is stored.
        /// </summary>
        protected abstract string GetFileName();

        private string FileName => _testMode
            ? $"Test{GetFileName()}"
            : GetFileName();

        private static string DirectoryPath => Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "TableSoccer");

        private string FilePath => Path.Combine(DirectoryPath, FileName);

        /// <summary>
        /// Initializes a new instance of the <see cref="FilePersistence"/> class.
        /// </summary>
        /// <param name="testMode">if set to <c>true</c> a new file will be created and finally disposed.</param>
        protected FilePersistence(bool testMode = false) => _testMode = testMode;

        /// <summary>
        /// Reads the text from the file.
        /// </summary>
        protected string[] ReadData()
        {
            if (!Directory.Exists(DirectoryPath)) Directory.CreateDirectory(DirectoryPath);
            return File.Exists(FilePath)
                ? File.ReadAllLines(FilePath)
                : new string[0];
        }

        /// <summary>
        /// Stores all the data to file.
        /// </summary>
        protected void StoreAllData(params string[] lines)
        {
            if (!Directory.Exists(DirectoryPath)) Directory.CreateDirectory(DirectoryPath);
            if (File.Exists(FilePath)) File.Delete(FilePath);

            File.WriteAllLines(FilePath, lines);
        }

        /// <summary>
        /// Stores the new lines to file.
        /// </summary>
        protected void StoreNewData(params string[] lines)
        {
            if (!Directory.Exists(DirectoryPath)) Directory.CreateDirectory(DirectoryPath);
            File.AppendAllLines(FilePath, lines);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (_testMode)
            {
                File.Delete(FilePath);
            }
        }
    }
}