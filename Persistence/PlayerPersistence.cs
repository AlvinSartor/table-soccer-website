﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Newtonsoft.Json;

namespace Persistence
{
    public sealed class PlayerPersistence : FilePersistence
    {
        private readonly Dictionary<string, Player> _players;

        /// <summary>
        /// Gets the name of the file where the information is stored.
        /// </summary>
        /// <returns></returns>
        protected override string GetFileName() => "Players";

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerPersistence"/> class.
        /// </summary>
        public PlayerPersistence(bool testMode = false)
            : base(testMode)
        {
            _players = ReadData().Select(GetDetails).ToDictionary(x => x.Id, x => x);
        }

        /// <summary>
        /// Fetches all stored players.
        /// </summary>
        public IEnumerable<Player> FetchAllPlayers() => _players.Values;

        /// <summary>
        /// Stores the new player.
        /// </summary>
        /// <param name="player">The player to store.</param>
        /// <exception cref="System.Exception">Player with id {player.Id} already exists.</exception>
        public void StorePlayer(Player player)
        {
            if (_players.ContainsKey(player.Id))
            {
                throw new Exception($"Player with id {player.Id} already exists.");
            }

            _players.Add(player.Id, player);
            StoreNewData(GetLine(player));
        }

        /// <summary>
        /// Removes the player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <exception cref="System.Exception">Player with id {player.Id} does not exist.</exception>
        public void RemovePlayer(Player player)
        {
            if (!_players.ContainsKey(player.Id))
            {
                throw new Exception($"Player with id {player.Id} does not exist.");
            }

            _players.Remove(player.Id);
            StoreAllData(_players.Values.Select(GetLine).ToArray());
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public new void Dispose()
        {
            base.Dispose();
        }

        private static string GetLine(Player details) => JsonConvert.SerializeObject(details);

        private static Player GetDetails(string line) => JsonConvert.DeserializeObject<Player>(line);
    }
}